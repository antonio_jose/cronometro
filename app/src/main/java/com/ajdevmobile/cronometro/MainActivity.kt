package com.ajdevmobile.cronometro

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.widget.Button
import android.widget.Chronometer

class MainActivity : AppCompatActivity() {

    lateinit var cronometro : Chronometer
    lateinit var bt_iniciar : Button
    lateinit var bt_pausar : Button
    lateinit var bt_zerar : Button

    var executando = false
    var pause : Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cronometro = findViewById(R.id.cronometro)
        bt_iniciar = findViewById(R.id.bt_start)
        bt_pausar = findViewById(R.id.bt_stop)
        bt_zerar = findViewById(R.id.bt_zerar)


        bt_iniciar.setOnClickListener {
            iniciarCronometro()
        }

        bt_pausar.setOnClickListener {
             pausarCronometro()
        }

        bt_iniciar.setOnClickListener {
            zerarCronometro()
        }

    }

    fun iniciarCronometro(){

        if(!executando){
            cronometro.base = SystemClock.elapsedRealtime() - pause
            cronometro.start()
            executando = true
        }


    }

    fun pausarCronometro(){

        if(executando){
            cronometro.stop()
            pause = SystemClock.elapsedRealtime() - cronometro.base
            executando = false
        }

    }

    fun zerarCronometro(){

        cronometro.base = SystemClock.elapsedRealtime()
        pause = 0
    }


}